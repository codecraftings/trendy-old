package com.trendy.db.models

import argonaut._
import Argonaut._

object JsonCodecHelper {

  implicit class FieldsEnum(enumeration: Enumeration) {
    def at(i: Int): String = {
      enumeration(i).toString
    }
  }

  def codec1[A, X, TField <: Enumeration](f: (A) => X, g: (X) => Option[(A)])
                                         (fields: TField)
                                         (implicit eA:EncodeJson[A], dA:DecodeJson[A]): CodecJson[X] = {
    require(fields.values.size == 1)
    casecodec1(f, g)(fields.at(0))(eA, dA)
  }

  def codec2[A, B, X, TField <: Enumeration](f: (A, B) => X, g: (X) => Option[(A, B)])
                                            (fields: TField)
                                            (implicit eA:EncodeJson[A], dA:DecodeJson[A], eB:EncodeJson[B], dB:DecodeJson[B]): CodecJson[X] = {
    require(fields.values.size == 2)
    casecodec2(f, g)(fields.at(0), fields.at(1))(eA, dA, eB, dB)
  }

  def codec3[A, B, C, X, TField <: Enumeration](f: (A, B, C) => X, g: (X) => Option[(A, B, C)])
                                               (fields: TField)
                                               (implicit eA:EncodeJson[A], dA:DecodeJson[A], eB:EncodeJson[B], dB:DecodeJson[B], eC: EncodeJson[C], dC: DecodeJson[C]): CodecJson[X] = {
    require(fields.values.size == 3)
    casecodec3(f, g)(fields.at(0), fields.at(1), fields.at(2))(eA, dA,eB,dB,eC,dC)
  }

  def codec4[A, B, C, D, X, TField <: Enumeration](f: (A, B, C, D) => X, g: (X) => Option[(A, B, C, D)])
                                                  (fields: TField)
                                                  (implicit eA:EncodeJson[A], dA:DecodeJson[A], eB:EncodeJson[B], dB:DecodeJson[B], eC:EncodeJson[C], dC:DecodeJson[C], eD:EncodeJson[D], dD:DecodeJson[D]): CodecJson[X] = {
    require(fields.values.size == 4)
    casecodec4(f, g)(fields.at(0), fields.at(1), fields.at(2), fields.at(3))(eA, dA, eB, dB, eC,dC, eD, dD)
  }

  def codec5[A, B, C, D, E, X, TField <: Enumeration](f: (A, B, C, D, E) => X, g: (X) => Option[(A, B, C, D, E)])
                                                     (fields: TField)
                                                     (implicit eA:EncodeJson[A], dA:DecodeJson[A], eB:EncodeJson[B], dB:DecodeJson[B], eC:EncodeJson[C], dC:DecodeJson[C], eD:EncodeJson[D], dD:DecodeJson[D], eE:EncodeJson[E], dE:DecodeJson[E]): CodecJson[X] = {
    require(fields.values.size == 5)
    casecodec5(f, g)(fields.at(0), fields.at(1), fields.at(2), fields.at(3), fields.at(4))(eA, dA, eB, dB, eC, dC, eD, dD, eE, dE)
  }

  def codec6[A, B, C, D, E, F, X, TField <: Enumeration](f: (A, B, C, D, E, F) => X, g: (X) => Option[(A, B, C, D, E, F)])
                                                        (fields: TField)
                                                        (implicit eA:EncodeJson[A], dA:DecodeJson[A], eB:EncodeJson[B], dB:DecodeJson[B], eC:EncodeJson[C], dC:DecodeJson[C], eD:EncodeJson[D], dD:DecodeJson[D], eE:EncodeJson[E], dE:DecodeJson[E], eF:EncodeJson[F], dF:DecodeJson[F]): CodecJson[X] = {
    require(fields.values.size == 6)
    casecodec6(f, g)(fields.at(0), fields.at(1), fields.at(2), fields.at(3), fields.at(4), fields.at(5))(eA, dA, eB, dB, eC, dC, eD, dD, eE, dE, eF, dF)
  }
}
