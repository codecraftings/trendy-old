package com.trendy.db.MongoDB

import org.mongodb.scala._

/**
  * Created by amish on 12/29/2015.
  */
package object MongoDB {
  val client = MongoClient()
  val db = client.getDatabase("fbData");

  def getCollection(name: String): MongoCollection[Document] = {
    db.getCollection(name)
  }
}