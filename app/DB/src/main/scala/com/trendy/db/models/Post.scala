package com.trendy.db.models
import argonaut._
import Argonaut._
case class Post(val id:String, val name:String) {

}
object Post{
  implicit def PostCode: CodecJson[Post] = casecodec2(Post.apply, Post.unapply)("id", "name")
}