package com.trendy.db.models

import argonaut._
import Argonaut._

case class Page(val ID: String, val Name: String, val Country: String, val LikesCount: Long, val LastPostHarvest: Boolean = false) {

}

object Page {

  object Fields extends Enumeration {
    val PageId = Value("_id")
    val PageName = Value("page_name")
    val Country = Value("country")
    val LikesCount = Value("likes_count")
    val LastPostHarvest = Value("last_post_harvest")
  }

  implicit def PageCode: CodecJson[Page] = JsonCodecHelper.codec5(Page.apply, Page.unapply)(Fields)

  def toJson(p: Page): String = {
    p.jencode.toString()
  }

  def fromJson(json: String): Option[Page] = {
    Parse.decodeOption[Page](json)
  }
}


