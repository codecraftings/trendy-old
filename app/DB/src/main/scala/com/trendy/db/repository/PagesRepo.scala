package com.trendy.db.repository

import com.mongodb.client.model.Filters
import com.trendy.db.MongoDB.{Implicits, MongoDB}
import com.trendy.db.models.Page
import org.mongodb.scala.MongoCollection
import org.mongodb.scala.bson.collection.immutable.Document
import rx.lang.{scala => rx}
import Implicits._
abstract class PagesRepo {
  val dbCollection: MongoCollection[Document];

  implicit def documentToPage(doc: Document): Page = {
    Page.fromJson(doc.toJson()).get
  }

  implicit def pageToDocument(page: Page): Document = {
    Document(Page.toJson(page))
  }

  def create(page: Page):rx.Observable[Page] = {
    dbCollection.insertOne(page).map(_ => page)
  }

  def get(id: String):rx.Observable[Page] = {
    dbCollection.find(Filters.eq(Page.Fields.PageId.toString, id)).map(documentToPage(_))
  }
}

object PagesRepo extends PagesRepo {
  override val dbCollection: MongoCollection[Document] = MongoDB.getCollection("pages")
}
