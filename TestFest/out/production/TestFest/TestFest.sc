import scala.collection.mutable.ListBuffer

var ll = System.nanoTime()
def l(): Unit = {
  println(System.nanoTime() - ll)
  ll = System.nanoTime()
}
def encode[A](ls:List[A]):List[(Int, A)] = {
  var last:A = ls.head
  var count = 1
  val encoded = new ListBuffer[(Int, A)]
  for(l <- ls.tail){
    if(l==last){
      count += 1
    }else{
      encoded.append((count, last))
      last = l
      count = 1
    }
  }
  encoded.append((count, last))
  encoded.toList
}

def encodeDirect[A](ls:List[A]):List[(Int, A)] = ls match{
  case Nil => Nil
  case h::tail => {
    val (m, next) = ls.span(_==h)
    (m.length, m.head)::encodeDirect(next)
  }
}
l()
encode(List('a','a','a','b','c','d','d','d','d','e','e','f','g'))
l()
encodeDirect(List('a','a','a','b','c','d','d','d','d','e','e','f','g'))
l()
