import scala.collection.mutable.ListBuffer

/**
  * Created by amish on 1/10/2016.
  */
object Price {
  def main(args: Array[String]): Unit = {
    val T = Console.in.readLine().toInt
    for(t <- 0 until T){
      val arr = Console.in.readLine().split(' ')
      val n = arr(0).toInt
      val H = arr(1).toInt
      val boxes = Console.in.readLine().split(' ')
      var bucket = List[Int]()
      var numOfWin = 0
      for(i <- 0 until n){
        val boxVal = boxes(i).toInt
        bucket = boxVal::bucket
        var count = 0;
        bucket = bucket.takeWhile(item =>{
          count += item
          if(count>H){
            count -= item
            false
          }else{
            true
          }
        })
        if(count>0){
          numOfWin += bucket.length
        }
      }
      println("Case #"+(t+1)+": "+numOfWin)
    }
  }
}
